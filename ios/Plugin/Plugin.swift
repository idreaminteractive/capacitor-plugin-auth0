import Foundation
import Capacitor
import Auth0
/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(Auth0Plugin)
public class Auth0Plugin: CAPPlugin {
    
    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.success([
            "value": value
        ])
    }
    
    @objc func auth(_ call: CAPPluginCall) {
        // HomeViewController.swift

        Auth0
            .webAuth()
            .scope("openid profile")
            .audience("https://betamerica-dev.us.auth0.com/userinfo")
            .start { result in
                switch result {
                case .failure(let error):
                    // Handle the error
                    print("Error: \(error)")
                case .success(let credentials):
                    // Do something with credentials e.g.: save them.
                    // Auth0 will automatically dismiss the login page
//                    credentials.
                    print("Credentials: \(credentials.dictionaryWithValues(forKeys: ["accessToken", "idToken", "refreshToken"]))")
                }
        }
    }
}
