import BaseError from "./baseError";
export default class ParameterError extends BaseError {
    expected: any;
    actual: any;
    missing: any;
    constructor(expected: any, actual: any, missing: any);
}
export declare function apply(rules: any, values: any): {};
