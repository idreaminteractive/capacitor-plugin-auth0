import BaseError from "../utils/baseError";
export default class AuthError extends BaseError {
    json: any;
    status: any;
    constructor(response: any);
}
