import BaseError from "../utils/baseError";
export default class Auth0Error extends BaseError {
    json: any;
    status: any;
    code: any;
    constructor(response: any);
}
