/**
 * Auth0 Auth API
 *
 * @export Auth
 * @see https://auth0.com/docs/api/authentication
 * @class Auth
 */
export default class Auth {
    client: any;
    domain: any;
    clientId: any;
    constructor(options?: any);
    /**
     * Builds the full authorize endpoint url in the Authorization Server (AS) with given parameters.
     *
     * @param {Object} parameters parameters to send to `/authorize`
     * @param {String} parameters.responseType type of the response to get from `/authorize`.
     * @param {String} parameters.redirectUri where the AS will redirect back after success or failure.
     * @param {String} parameters.state random string to prevent CSRF attacks.
     * @returns {String} authorize url with specified parameters to redirect to for AuthZ/AuthN.
     * @see https://auth0.com/docs/api/authentication#authorize-client
     *
     * @memberof Auth
     */
    authorizeUrl(parameters?: {}): any;
    /**
     * Builds the full logout endpoint url in the Authorization Server (AS) with given parameters.
     *
     * @param {Object} parameters parameters to send to `/v2/logout`
     * @param {Boolean} [parameters.federated] if the logout should include removing session for federated IdP.
     * @param {String} [parameters.clientId] client identifier of the one requesting the logout
     * @param {String} [parameters.returnTo] url where the user is redirected to after logout. It must be declared in you Auth0 Dashboard
     * @returns {String} logout url with specified parameters
     * @see https://auth0.com/docs/api/authentication#logout
     *
     * @memberof Auth
     */
    logoutUrl(parameters?: {}): any;
    /**
     * Exchanges a code obtained via `/authorize` (w/PKCE) for the user's tokens
     *
     * @param {Object} parameters parameters used to obtain tokens from a code
     * @param {String} parameters.code code returned by `/authorize`.
     * @param {String} parameters.redirectUri original redirectUri used when calling `/authorize`.
     * @param {String} parameters.verifier value used to generate the code challenge sent to `/authorize`.
     * @returns {Promise}
     * @see https://auth0.com/docs/api-auth/grant/authorization-code-pkce
     *
     * @memberof Auth
     */
    exchange(parameters?: {}): any;
    /**
     * Exchanges an external token obtained via a native social authentication solution for the user's tokens
     *
     * @param {Object} parameters parameters used to obtain user tokens from an external provider's token
     * @param {String} parameters.subjectToken token returned by the native social authentication solution
     * @param {String} parameters.subjectTokenType identifier that indicates the native social authentication solution
     * @param {Object} [parameters.userProfile] additional profile attributes to set or override, only on select native social authentication solutions
     * @param {String} [parameters.audience] API audience to request
     * @param {String} [parameters.scope] scopes requested for the issued tokens. e.g. `openid profile`
     * @returns {Promise}
     *
     * @see https://auth0.com/docs/api/authentication#token-exchange-for-native-social
     *
     * @memberof Auth
     */
    exchangeNativeSocial(parameters?: {}): any;
    /**
     * Performs Auth with user credentials using the Password Realm Grant
     *
     * @param {Object} parameters password realm parameters
     * @param {String} parameters.username user's username or email
     * @param {String} parameters.password user's password
     * @param {String} parameters.realm name of the Realm where to Auth (or connection name)
     * @param {String} [parameters.audience] identifier of Resource Server (RS) to be included as audience (aud claim) of the issued access token
     * @param {String} [parameters.scope] scopes requested for the issued tokens. e.g. `openid profile`
     * @returns {Promise}
     * @see https://auth0.com/docs/api-auth/grant/password#realm-support
     *
     * @memberof Auth
     */
    passwordRealm(parameters?: {}): any;
    /**
     * Obtain new tokens using the Refresh Token obtained during Auth (requesting `offline_access` scope)
     *
     * @param {Object} parameters refresh token parameters
     * @param {String} parameters.refreshToken user's issued refresh token
     * @param {String} [parameters.scope] scopes requested for the issued tokens. e.g. `openid profile`
     * @returns {Promise}
     * @see https://auth0.com/docs/tokens/refresh-token/current#use-a-refresh-token
     *
     * @memberof Auth
     */
    refreshToken(parameters?: {}): any;
    /**
     * Starts the Passworldess flow with an email connection
     *
     * @param {Object} parameters passwordless parameters
     * @param {String} parameters.email the email to send the link/code to
     * @param {String} parameters.send the passwordless strategy, either 'link' or 'code'
     * @param {String} parameters.authParams optional parameters, used when strategy is 'linḱ'
     * @returns {Promise}
     *
     * @memberof Auth
     */
    passwordlessWithEmail(parameters?: {}): any;
    /**
     * Starts the Passworldess flow with an SMS connection
     *
     * @param {Object} parameters passwordless parameters
     * @param {String} parameters.phoneNumber the phone number to send the link/code to
     * @returns {Promise}
     *
     * @memberof Auth
     */
    passwordlessWithSMS(parameters?: {}): any;
    /**
     * Finishes the Passworldess authentication with an email connection
     *
     * @param {Object} parameters passwordless parameters
     * @param {String} parameters.email the email where the link/code was received
     * @param {String} parameters.code the code numeric value (OTP)
     * @param {String} parameters.audience optional API audience to request
     * @param {String} parameters.scope optional scopes to request
     * @returns {Promise}
     *
     * @memberof Auth
     */
    loginWithEmail(parameters?: {}): any;
    /**
     * Finishes the Passworldess authentication with an SMS connection
     *
     * @param {Object} parameters passwordless parameters
     * @param {String} parameters.phoneNumber the phone number where the code was received
     * @param {String} parameters.code the code numeric value (OTP)
     * @param {String} parameters.audience optional API audience to request
     * @param {String} parameters.scope optional scopes to request
     * @returns {Promise}
     *
     * @memberof Auth
     */
    loginWithSMS(parameters?: {}): any;
    /**
     * Revoke an issued refresh token
     *
     * @param {Object} parameters revoke token parameters
     * @param {String} parameters.refreshToken user's issued refresh token
     * @returns {Promise}
     *
     * @memberof Auth
     */
    revoke(parameters?: {}): any;
    /**
     * Return user information using an access token
     *
     * @param {Object} parameters user info parameters
     * @param {String} parameters.token user's access token
     * @returns {Promise}
     *
     * @memberof Auth
     */
    userInfo(parameters?: {}): any;
    /**
     * Request an email with instructions to change password of a user
     *
     * @param {Object} parameters reset password parameters
     * @param {String} parameters.email user's email
     * @param {String} parameters.connection name of the connection of the user
     * @returns {Promise}
     *
     * @memberof Auth
     */
    resetPassword(parameters?: {}): any;
    /**
     *
     *
     * @param {Object} parameters create user parameters
     * @param {String} parameters.email user's email
     * @param {String} [parameters.username] user's username
     * @param {String} parameters.password user's password
     * @param {String} parameters.connection name of the database connection where to create the user
     * @param {String} [parameters.metadata] additional user information that will be stored in `user_metadata`
     * @returns {Promise}
     *
     * @memberof Auth
     */
    createUser(parameters?: {}): any;
}
