export default class Client {
    telemetry: any;
    baseUrl: any;
    bearer: any;
    domain: any;
    constructor(options: any);
    post(path: any, body: any): Promise<{
        json: any;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    }>;
    patch(path: any, body: any): Promise<{
        json: any;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    }>;
    get(path: any, query: any): Promise<{
        json: any;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    }>;
    url(path: any, query?: any, includeTelemetry?: boolean): any;
    request(method: any, url: any, body: any): Promise<{
        json: any;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    } | {
        text: string;
        status: number;
        ok: boolean;
        headers: Headers;
    }>;
    _encodedTelemetry(): string;
}
