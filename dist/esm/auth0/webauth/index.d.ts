/**
 * Helper to perform Auth against Auth0 hosted login page
 *
 * It will use `/authorize` endpoint of the Authorization Server (AS)
 * with Code Grant and Proof Key for Challenge Exchange (PKCE).
 *
 * @export
 * @class WebAuth
 * @see https://auth0.com/docs/api-auth/grant/authorization-code-pkce
 */
export default class WebAuth {
    client: any;
    domain: any;
    clientId: any;
    agent: any;
    constructor(auth: any);
    /**
     * Starts the AuthN/AuthZ transaction against the AS in the in-app browser.
     *
     * In iOS it will use `SFSafariViewController` and in Android Chrome Custom Tabs.
     *
     * To learn more about how to customize the authorize call, check the Universal Login Page
     * article at https://auth0.com/docs/hosted-pages/login
     *
     * @param {Object}  parameters Parameters to send on the AuthN/AuthZ request.
     * @param {String}  [parameters.state] Random string to prevent CSRF attacks and used to discard unexepcted results. By default its a cryptographically secure random.
     * @param {String}  [parameters.nonce] Random string to prevent replay attacks of id_tokens.
     * @param {String}  [parameters.audience] Identifier of Resource Server (RS) to be included as audience (aud claim) of the issued access token
     * @param {String}  [parameters.scope] Scopes requested for the issued tokens. e.g. `openid profile`
     * @param {String}  [parameters.connection] The name of the identity provider to use, e.g. "google-oauth2" or "facebook". When not set, it will display Auth0's Universal Login Page.
     * @param {Number}  [parameters.max_age] The allowable elapsed time in seconds since the last time the user was authenticated (optional).
     * @param {Object}  options Other configuration options.
     * @param {Number}  [options.leeway] The amount of leeway, in seconds, to accommodate potential clock skew when validating an ID token's claims. Defaults to 60 seconds if not specified.
     * @param {Boolean} [options.ephemeralSession] Disable Single-Sign-On (SSO). It only affects iOS with versions 13 and above.
     * @returns {Promise}
     * @see https://auth0.com/docs/api/authentication#authorize-client
     *
     * @memberof WebAuth
     */
    authorize(parameters?: any, options?: any): any;
    /**
     *  Removes Auth0 session and optionally remove the Identity Provider session.
     *
     *  In iOS it will use `SFSafariViewController` and in Android Chrome Custom Tabs.
     *
     * @param {Object} parameters Parameters to send
     * @param {Bool} [parameters.federated] Optionally remove the IdP session.
     * @returns {Promise}
     * @see https://auth0.com/docs/logout
     *
     * @memberof WebAuth
     */
    clearSession(options?: any): any;
}
