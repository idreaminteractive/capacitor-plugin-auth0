export default class Agent {
    show(url: any, ephemeralSession?: boolean, closeOnLoad?: boolean): Promise<unknown>;
    newTransaction(): Promise<unknown>;
}
