var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Plugins } from "@capacitor/core";
const { Device, App } = Plugins;
export default class Agent {
    show(url, ephemeralSession = false, closeOnLoad = false) {
        if (!Plugins.Auth0Plugin) {
            return Promise.reject(new Error("Missing NativeModule. React Native versions 0.60 and up perform auto-linking. Please see https://github.com/react-native-community/cli/blob/master/docs/autolinking.md."));
        }
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const platform = (yield Device.getInfo()).platform;
            const urlHandler = (event) => {
                console.warn('e', event);
                // App.removeListener('appUrlOpen', urlHandler);
                resolve(event.url);
            };
            console.log('-->', (yield App.getLaunchUrl()));
            const params = platform === "ios" ? [ephemeralSession, closeOnLoad] : [closeOnLoad];
            console.log(url, params);
            App.addListener('appUrlOpen', urlHandler);
            Plugins.Auth0Plugin.showUrl({ url }).then((data) => {
                // App.removeListener('appUrlOpen');
                console.log('---', data);
                if (data && data.error) {
                    reject(data);
                }
                else if (closeOnLoad) {
                    resolve();
                }
                else {
                    resolve(data);
                }
            }).catch((err) => {
                reject(err);
            });
        }));
    }
    newTransaction() {
        if (!Plugins.Auth0Plugin) {
            return Promise.reject(new Error("Missing NativeModule. React Native versions 0.60 and up perform auto-linking. Please see https://github.com/react-native-community/cli/blob/master/docs/autolinking.md."));
        }
        return new Promise((resolve, _reject) => {
            Plugins.Auth0Plugin.oauthParameters().then((params) => resolve(params));
        });
    }
}
//# sourceMappingURL=agent.js.map