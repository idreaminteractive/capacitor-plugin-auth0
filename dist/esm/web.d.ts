import { WebPlugin } from '@capacitor/core';
import { Auth0Plugin } from './definitions';
export declare class Auth0Web extends WebPlugin implements Auth0Plugin {
    constructor();
    getConstants(): Promise<import("./definitions").Constants>;
    showUrl(_options: {
        url: string;
    }): Promise<any>;
    oauthParameters(): Promise<import("./definitions").OAuthParameters>;
}
declare const Auth0: Auth0Web;
export { Auth0 };
