import { WebPlugin } from '@capacitor/core';
export class Auth0Web extends WebPlugin {
    constructor() {
        super({
            name: 'Auth0',
            platforms: ['web']
        });
    }
    getConstants() {
        throw new Error("Method not implemented.");
    }
    showUrl(_options) {
        throw new Error("Method not implemented.");
    }
    oauthParameters() {
        throw new Error("Method not implemented.");
    }
}
const Auth0 = new Auth0Web();
export { Auth0 };
import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(Auth0);
//# sourceMappingURL=web.js.map