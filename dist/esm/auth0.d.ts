import Users from "./auth0/management/users";
/**
 * Auth0 for React Native client
 *
 * @export
 * @class Auth0
 */
export default class Auth0 {
    auth: any;
    webAuth: any;
    options: any;
    /**
     * Creates an instance of Auth0.
     * @param {Object} options your Auth0 application information
     * @param {String} options.domain your Auth0 domain
     * @param {String} options.clientId your Auth0 application client identifier
     *
     * @memberof Auth0
     */
    constructor(options?: any);
    /**
     * Creates a Users API client
     * @param  {String} token for Management API
     * @return {Users}
     */
    users(token: string): Users;
}
