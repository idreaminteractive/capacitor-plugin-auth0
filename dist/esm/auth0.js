var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import Auth from "./auth0/auth";
import Users from "./auth0/management/users";
import WebAuth from "./auth0/webauth";
/**
 * Auth0 for React Native client
 *
 * @export
 * @class Auth0
 */
export default class Auth0 {
    /**
     * Creates an instance of Auth0.
     * @param {Object} options your Auth0 application information
     * @param {String} options.domain your Auth0 domain
     * @param {String} options.clientId your Auth0 application client identifier
     *
     * @memberof Auth0
     */
    constructor(options = {}) {
        const { domain, clientId } = options, extras = __rest(options, ["domain", "clientId"]);
        this.auth = new Auth(Object.assign({ baseUrl: domain, clientId }, extras));
        this.webAuth = new WebAuth(this.auth);
        this.options = options;
    }
    /**
     * Creates a Users API client
     * @param  {String} token for Management API
     * @return {Users}
     */
    users(token) {
        const _a = this.options, { domain, clientId } = _a, extras = __rest(_a, ["domain", "clientId"]);
        return new Users(Object.assign(Object.assign({ baseUrl: domain, clientId }, extras), { token }));
    }
}
//# sourceMappingURL=auth0.js.map